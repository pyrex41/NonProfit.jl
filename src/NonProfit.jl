module NonProfit

using CSV, DataFrames, Transducers, DataFramesMeta, Statistics, MultivariateStats,
    PrettyTables, Printf, GLM, Plots, Distributions, BlackBoxOptim
export getsumstats, getsortsumstats, loaddatainit, filterrawdata, process_more, loaddata,
    vret, applybygroup,
    programratio, diffretbygroup, deltadata, filtersequence, regression, estimate_qd,
    yqd, PCR, mywhere, viewN, psumtable, winsorize, qdda, grouptable,filternan, lag_charitable_percent
import Base.Threads.@threads

include("Functions.jl")

function loaddatainit(fm="/Users/reuben/Downloads/nccs.corePfFyTrend201509.csv")
    df = CSV.File(fm) |> DataFrame

    df_nozero = let
	dff = @where(dropmissing(df, :P2tasFmv, disallowmissing=true), :P2tasFmv .> 1e4) # remove entries w/ asssets < 1000
	dff = @where dff :OUTNCCS .== "IN"  # remove entries w/ out of scope flag
	dff = @where dropmissing(dff, :FNDNCD, disallowmissing=true) :FNDNCD .== 4 # keep only entries that are non-operating foundations
	dff = @where dff :FISYR .> 1993 # seem to be changes to the formss affter 1993,
	dff = @where dff :FISYR .< 2014 #  incomplete data in 2014
    end
end
function filterrawdata(df_nozero)
    dfn = DataFrame()
    dfn[!,:FISYR] = df_nozero.FISYR
    dfn[!,:EIN] = df_nozero.EIN
    dfn[!,:ASSETS] = df_nozero.P2tasFmv
    dfn[!,:SIZE] = df_nozero.P2tasFmv |> Map(log) |> collect
    dfn[!,:OVERHEAD_dollar] = df_nozero.P1AdmExp
    dfn[!,:OVERHEAD] = dfn.OVERHEAD_dollar ./ dfn.ASSETS
    dfn[!,:DONATE] = df_nozero.P1TCONT |> Map(x->Int(x>0)) |> collect
    dfn[!,:DONATIONS] = df_nozero.P1TCONT
    dfn[!,:DONATE_PCT] = df_nozero.P1TCONT ./ dfn.ASSETS
    dfn[!,:ASSETS_NET] = (df_nozero.P2tasFmv .- df_nozero.P2TLIABL)
    dfn[!,:STOCK] = df_nozero.P2CRPSTK ./ df_nozero.P2TINVSC
    dfn[!,:GRANTS] = df_nozero.P1CONTPD
    dfn[!,:PROGRAM] = df_nozero.P1TEXMEX
    dfn[!,:CEO_dollar] = df_nozero.P1OFCOMP
    dfn[!,:CEO] = df_nozero.P1OFCOMP ./ df_nozero.P1TOTEXP
    dfn[!,:TOTALEXPENSES] = df_nozero.P1TOTEXP
    dfn[!,:REVENUE] = df_nozero.P1TOTREV
    dfn[!,:REVENUE_INVESTMENT] = df_nozero.P1NETINV
    dfn[!,:GRANT_RATIO] = dfn.GRANTS ./ dfn.TOTALEXPENSES
    dfn[!,:DA] = .05 * dfn.ASSETS
    dfn[!,:PAYOUT_RATIO] = dfn.PROGRAM ./ dfn.DA
    let
        gd = groupby(dfn, :EIN)
        f(arr) = vcat([NaN],diff(arr) ./ arr[1:end-1])
	dfn[!,:GROWTH] = foldxt(vcat, 1:length(gd) |> Map(i->gd[i]) |>
                                Map(x->x.ASSETS) |> Map(f))
        dfn[!,:GROWTH_NET] = foldxt(vcat, 1:length(gd) |> Map(i->gd[i]) |>
                                    Map(x->x.ASSETS_NET) |> Map(f))
    end
    dfn[!,:CAPGAIN] = df_nozero.P1NGASTS ./ df_nozero.P1NETINV
    dfn
end

process_more = quote
    df3 = estimate_qd(df1, nmin=3)
    df4 = estimate_qd(df1, nmin=3, allfields=true)
    df5 = qdda(df4)
    df5 = qdda(df5, :GRANTS)
end

save_data = quote
    CSV.write("trimdata.csv", df1)
    CSV.write("eqd.csv", df3)
    CSV.write("eqd2.csv", df5)
end

function parsedata(fm="/Users/reuben/Downloads/nccs.corePfFyTrend201509.csv")
    df_nozero = loaddatainit(fm)
    df1 = filterrawdata(df_nozero)
    ld = quote
        df_nozero = $df_nozero
        df1 = $df1
        df3 = estimate_qd(df1, nmin=3)
        df4 = estimate_qd(df1, nmin=3, allfields=true)
        df5 = qdda(df4)
        df5 = qdda(df5, :GRANTS)
    end
    @eval Main $ld
    @eval Main $save_data
end

load_data_script = quote
    using CSV, DataFrames
    df1 = CSV.File("trimdata.csv") |> DataFrame
    df3 = CSV.File("eqd.csv") |> DataFrame
    df5 = CSV.File("eqd2.csv") |> DataFrame
end
loaddata() = @eval Main $load_data_script
end # end module
