function qdda(df::DataFrame, col=:PROGRAM)
    dfn = copy(df)
    f1(a1,a2) = pushfirst!(a1 ./ a2, NaN)
    function f1(g)
        a1 = g[2:end, col]
        a2 = g.DA[1:end-1]
        f1(a1,a2)
    end
    gd = groupby(dfn, :EIN)
    newcol = string(col, "_div_DA") |> Symbol
    dfn[!,newcol] = foldxt(vcat, 1:length(gd) |> Map(i->gd[i]) |>
                                Map(f1))
    dfn
end

function winsorize(vv::Vector, l=.01, h=.99)
    v = filter(!isnan,vv)
    ll = quantile(v, l)
    hh = quantile(v, h)
    filter(v) do i
        i > ll && i < hh
    end
end
winsorize(df::DataFrame) = winsorize(df, map(Symbol, names(df))...)
function winsorize(df::DataFrame, cols::Symbol...; low=.01, high=.99)
    dfa = copy(df)
    n = size(dfa)[1]
    lhs = map(cols) do col
        v = filter(!isnan, dfa[:,col])
        l = quantile(v, low)
        h = quantile(v, high)
        l,h
    end
    arr = map(1:length(cols)) do i
        col = cols[i]
        l,h = lhs[i]
        map(dfa[:,col]) do val
            val > l && val < h && !isnan(val)
        end
    end
    dfa[!, :Bool] = foldl((a,b)-> a .& b, arr)
    filter!(row -> row.Bool, dfa)
end

function winsorize(df::DataFrame, col::Symbol; low=.01, high=.99)
    dfa = copy(df)
    filter!(row->!isnan(row[col]), dfa)

    v = dfa[:,col]
    l = quantile(v, low)
    h = quantile(v, high)
    filter!(row -> row[col] > l && row[col] < h, dfa)
end

commas(x) = x
function commas(num::Float64; digits=2)
    nd = digits
    str = @sprintf "%0.8f" num
    parts = split(str, ".")
    ll = length(split(string(num), ".")[2])
    out = if num >= 1
        main = parts[1]
        dec_string1 = parts[2][1:min(nd+1, 8)]
        dec_num = parse(Float64, "."*dec_string1)
        dec_string = round(dec_num, digits=nd) |> string
        mainstring = replace(main, r"(?<=[0-9])(?=(?:[0-9]{3})+(?![0-9]))" => ",")
        string(mainstring, dec_string[2:end])
    else
        string(round(num, digits=2))
    end
    gap = nd - ll
    if gap > 0
        for i=1:gap
            out = string(out, "0")
        end
    end
    out
end
commas(num::Integer) = replace(string(num), r"(?<=[0-9])(?=(?:[0-9]{3})+(?![0-9]))" => ",")


psumtable(df1::DataFrame; kwargs...) = psumtable(df1, [:ASSETS, :ASSETS_NET, :SIZE, :OVERHEAD,:OVERHEAD_dollar,
                                            :DONATIONS, :DONATE, :DONATE_PCT, :GROWTH, :GROWTH_NET, :CAPGAIN, :CEO,
                                            :CEO_dollar, :GRANTS, :PROGRAM, :REVENUE, :REVENUE_INVESTMENT])
function psumtable(df1::DataFrame, cols::Vector{Symbol})
    dfout = getsumstats(df1, cols...)
    ptable(dfout)
end
function ptable(df1::DataFrame)
    data = Array(df1)
    header = (["Variables", "N_obs", "Mean", "25th", "Median", "75th", "Std. Dev"])
    ptable(data,header)
end
function formatter(v,i,j)
    if j == 2
        commas(v)
    elseif j>=3
        vv = round(v, digits=2)
        commas(vv)
    else
        v
    end
end

ptable(data, header) =  pretty_table(data, header=header, header_crayon = crayon"yellow bold", formatters = formatter, tf=tf_unicode_rounded)

function filtersequence(dfn::DataFrame, n::Integer=2)
    xdict = let
	xx = let
	    gd = groupby(dfn, :EIN)
	    isseqlist(v) = v[1:end-1] .+ 1 == circshift(v, -1)[1:end-1]
	    func(g) = size(g)[1] > n && isseqlist(g.FISYR) #
	    applybygroup(dfn, func; by=:EIN)
	end
	zip(dfn.EIN |> union, xx) |> Dict
    end
    filter(row-> xdict[row.EIN], dfn)
end

function deltadata(df1::DataFrame, n=8)
    dff = @where df1 :CEO_dollar .> 10^4*5
    dff = @where dff :REVENUE .!= 0
    dff = @where dff :PROGRAM .!= 0
    dff = @where dff :DONATIONS .!= 0
    dff = filtersequence(dff, n)
    dn = diffretbygroup(dff, g->vret(g.CEO_dollar), :ΔCOMP, by=:EIN)
    dn[!,:ΔREVENUE] = diffretbygroup(dff, g->vret(g.REVENUE), false, by=:EIN)
    dn[!,:ΔPROGRAMRATIO] = diffretbygroup(dff, programratio, false, by=:EIN)
    dn[!,:ΔDONATE] = diffretbygroup(dff, g->vret(g.DONATIONS), false, by=:EIN)
    dn[:, [:EIN, :FISYR, :ΔCOMP, :ΔREVENUE, :ΔPROGRAMRATIO, :ΔDONATE]]
end

function diffretbygroup(df::DataFrame, func::Function, coln; by::Symbol)
    gd = groupby(df, by)
    function mapfunc(g)
	gn = g[2:end,:]
	cnew = func(g)
	if coln==false
	    cnew
	else
	    gn[!, coln] = cnew
	    gn
	end
    end
    gdd = 1:length(gd) |> Map(i->gd[i]) |> Map(mapfunc) |> tcollect
    vcat(gdd...)
end

vret(v) = diff(v) ./ v[1:end-1]

programratio(p0, p1, r0, r1) = (p1/r1 - p0/r0) * r1/p0
programratio(t::Tuple) = programratio(t...)
function programratio(pp, rr)
    tt = zip(pp[1:end-1],pp[2:end], rr[1:end-1], rr[2:end])
    out = tt |> Map(programratio) |> collect
end
programratio(g::T) where {T<:SubDataFrame} = programratio(g.PROGRAM, g.REVENUE)

function applybygroup(df::DataFrame, func::Function; by::Symbol, collect::Function=collect)
    gd = groupby(df, by)
    1:length(gd) |> Map(i->gd[i]) |> Map(func) |> collect
end

function lag_charitable_percent(gd)
    out = gd.PROGRAM[2:end] ./ gd.ASSETS[1:end-1]
    pushfirst!(out, NaN)
    dfa = gd[:,:]
    dfa[!, :lag_charitable_percent] = out
    dfa
end

function getsortsumstats(df, col::Symbol; sortby::Symbol)
    labels = [:year, :mean, :p25, :median, :p75, :std]
    p25(x) = quantile(x, .25)
    p75(x) = quantile(x, .75)
    funcs = (mean, p25, median, p75, std)
    gg = groupby(df, sortby)
    mapfunc(func) = sort(combine(gg, col=>func),sortby)
    joinfunc(x...) = innerjoin(x...; on=sortby)
    out = foldl(joinfunc, funcs |> Map(mapfunc))
    rename!(out, labels)
end
getsumstats(df::DataFrame) = getsumstats(df, map(Symbol, names(df))...)
function getsumstats(df::DataFrame, cols::Symbol...)
    arrs = map(x-> getsumstats_helper(df,x), cols |> collect)
    getsumstats(arrs, cols...)
end
function getsumstats(arr::Array{<:Real}, cols::Symbol...)
    ii,_ = size(arr)
    arrs = map(1:ii) do i
        arr[i,:] |> Map(x->isnan(x) ? 0 : x) |> collect |> getsumstats_helper
    end
    getsumstats(arrs, cols...)
end
function getsumstats(arrs::Vector, cols::Symbol...)
    dics = map(Dict, arrs)
    out = DataFrame()
    out[!,:Variables] = cols |> collect
    kk = map(x->x[1], arrs[1])
    for k in kk
        out[!,k] = map(d->d[k], dics) |> collect
    end
    out
end
function getsumstats_helper(df::DataFrame, col::Symbol)
    ffunc(x) = !isnan(x) && x^2 != Inf
    column = filter(ffunc, df[:,col])
    getsumstats_helper(column)
end
function getsumstats_helper(column::Vector)
    labels = (:mean, :p25, :median, :p75, :std)
    p25(x) = quantile(x, .25)
    p75(x) = quantile(x, .75)
    funcs = (mean, p25, median, p75, std)
    function mapfunc(t::Tuple)
	l,f = t
	l => f(column)
    end
    arr = Vector{Pair{Symbol, Real}}()
    arr = vcat(arr,zip(labels, funcs) |> Map(mapfunc) |> collect)
    pushfirst!(arr, :n_observations => length(column))
    arr
end


function groupregression(formula, gd)
    n = formula.rhs |> length
    m(g) = lm(formula,g)
    function f(g)
        mm = m(g)
        p,_ = size(g)
        funcs = (coef, adjr2)
        foldl(vcat, funcs |> Map(f->f(mm)))
    end
    foldxt(hcat, 1:length(gd) |> Map(i->gd[i]) |> Map(f))
end


function estimate_qd(df::DataFrame, col::Symbol=:PROGRAM; nmin=8, allfields=false)
    dfa = copy(df)
    filter!(row -> row[col] >= 0, dfa)
    dfa = filtersequence(dfa, nmin)
    estimate_qd(groupby(dfa,:EIN), col; allfields=allfields)
end
function estimate_qd(gd::GroupedDataFrame{DataFrame}, col::Symbol; allfields::Bool)
    foldxt(vcat, 1:length(gd) |> Map(i->gd[i]) |> Map(g-> estimate_qd(g,col; allfields=allfields)))
end
function estimate_qd(g::SubDataFrame, col::Symbol; allfields::Bool)
    ts = foldl(hcat, zip(g.ASSETS, g[:,col]) |> Scan(yqd, (false, false, false, false, false, 0.0)))
    dfa = allfields ? g[:,:] : g[:, [:FISYR, :EIN, :ASSETS, col]]
    dfa[!, :penalty] = map(Bool,ts[1,:]) # |> Map(Bool) |> collect
    dfa[!, :carryover_bool] = map(Bool,ts[2,:]) # |> Map(Bool) |> collect
    dfa[!, :bqd] = ts[3,:]
    dfa[!, :carryover_pct] = ts[4,:]
    dfa[!, :delay] = ts[5,:]
    dfa[!, :carryover] = ts[6,:]
    dfa
end

function yqd(carry_tup, tup)
    _, _, _, _, _, carryover = carry_tup
    assets, grants = tup
    bqd = assets * .05
    penalty = grants < carryover
    current_portion = penalty ? grants : grants - carryover
    carryover_new = max(bqd-current_portion,0)
    carryover_pct = carryover_new > 0 ? carryover_new / assets : NaN
    delay = (carryover_new / bqd >= .9) |> Int
    [penalty, carryover_new > 0, bqd, carryover_pct, delay, carryover_new]
end

function PCR(df1::DataFrame, n=8)
    cols = [:ASSETS, :DONATIONS_RECEIVED, :OVERHEAD, :PROGRAM, :REVENUE, :CAPGAIN]
    func(df, col) = df[df[:,col] .!= 0, :]
    dff = cols |> Scan(func, df1) |> TakeLast(1) |> collect |> x->filtersequence(x[1],n)
    dn = diffretbygroup(dff, g->vret(g.DONATIONS_RECEIVED), :ΔDONATE, by=:EIN)
    col_out = vcat([:FISYR, :EIN, :ΔDONATE], cols)
    dn[:,col_out]
end

function viewN(df::DataFrame, cols...)
    gd = groupby(df, :FISYR)
    xdicts = map(cols) do col
        1:length(gd) |> Map(i->gd[i]) |> Map(g->g[:,[:FISYR, col]]) |> Map(dropmissing) |>
            Filter(g->size(g)[1] > 0) |> Map(g->g.FISYR[1] => size(g)[1]) |> Dict
    end
    p = plot(legend=:topleft, size=(1200,900), markersize=15)
    for (i,d) in enumerate(xdicts)
        scatter!(d, label=string(cols[i]))
    end
    p
end

function format_help(numb)
    snumb = @sprintf "%0.0f" numb
    nn = div(length(snumb), 3)
    if nn == 0
        @sprintf "%15i" numb
    elseif nn == 1
        @sprintf "%14i" numb
    elseif nn == 2
        @sprintf "%13i" numb
    elseif nn == 3
        @sprintf "%12i" numb
    elseif nn == 4
        @sprintf "%11i" numb
    else
        @sprintf "%10i" numb
    end
end

function grouptable(df::DataFrame, col::Symbol, sizes::Vector; sizecol=:ASSETS, pretty=true)
    sort!(sizes)
    s1 = sizes[1:end-1]
    s2 = sizes[2:end]

    mf(tup) = mf(tup...)
    mf(a,b) = filter(row -> row[sizecol] > a && row[sizecol] <= b, df)
    rows = zip(s1,s2) |> Map(mf) |> Map(df-> getsumstats_helper(df, col)) |> tcollect
    labels = map(zip(s1,s2)) do tup
        l,h = tup
        ll = format_help(l)
        lll =  replace(ll, r"(?<=[0-9])(?=(?:[0-9]{3})+(?![0-9]))" => ",")
        hh = format_help(h)
        hhh =  replace(hh, r"(?<=[0-9])(?=(?:[0-9]{3})+(?![0-9]))" => ",")
        string(lll, " < ASSETS <= ", hhh)
    end
    arrs = []
    for (i,lab) in enumerate(labels)
        push!(arrs, vcat([:size_partition => lab], rows[i]))
    end
    dics = map(Dict, arrs)
    out = DataFrame()
    kk = map(x->x[1], arrs[1])
    for k in kk
        out[!,k] = map(d->d[k], dics) |> collect
    end
    if pretty
        pretty_table(Array(out), header=names(out), header_crayon = crayon"yellow bold", formatters = ft_printf("%5.1f", 3:8), tf=tf_unicode_rounded)
    else
        out
    end
end

filternan(df::DataFrame) = filternan(df, map(Symbol, names(df))...)
function filternan(df::DataFrame, cols::Symbol...)
    dfa = copy(df)
    cols |> Map(col -> filter!(row->!isnan(row[col]), dfa)) |> collect
    dfa
end
replacenan(df::DataFrame) = replacenan(df, map(Symbol, names(df))...)
function replacenan(df::DataFrame, cols::Symbol...)
    dfa = DataFrame()
    function func(col)
        dfa[!,col] = map(df[:,col]) do x
            isnan(x) ? 0.0 : x
        end
    end
    cols |> Map(func) |> tcollect
    dfa
end
function cleannan(form::GLM.FormulaTerm, df::DataFrame; zeronan=true)
    f(t::Term) = t.sym
    f(t::ConstantTerm) = t.n
    f(t::Tuple) = map(f, t) |> collect
    vars = vcat(form.lhs |> f, form.rhs |> f)
    filter!(x->typeof(x) == Symbol, vars)
    zeronan ? replacenan(df, vars...) : filternan(df, vars...)
end

function regression(df::DataFrame, form::GLM.FormulaTerm; zeronan=true)
    dfa = cleannan(form, df, zeronan=zeronan)
    @show size(dfa)
    lm(form, dfa)
end

function partitiondf_helper(df::DataFrame, sizes::Vector{<:Real}, sizecol::Symbol)
    mf(tup) = mf(tup...)
    mf(a,b) = filter(row -> row[sizecol] > a && row[sizecol] <= b, df)
    sort!(sizes)
    s1 = sizes[1:end-1]
    s2 = sizes[2:end]
    zip(s1,s2)  |> Map(mf)
end

function partitiondf(df::DataFrame, sizes::Vector{<:Real}, sizecol::Symbol=:ASSETS)
    ibegin = partitiondf_helper(df, sizes, sizecol)
    ibegin |> tcollect
end
function partitiondf(func::Function, df::DataFrame, sizes::Vector{<:Real}, sizecol::Symbol=:ASSETS)
    ibegin = partitiondf_helper(df, sizes, sizecol)
    ibegin |> Map(func) |> tcollect
end


export revenuetotals, fixedopt, plotmc, randopt, mcmean, meta_mc, montecarlo, unzip, pmc,
    lagdiv, total_giving

lagdiv(v::Vector, w::Vector) = v[2:end] ./ w[1:end-1]

function revenuetotals(df::DataFrame)
    cols = [:FISYR, :ASSETS_NET, :REVENUE, :DONATIONS, :REVENUE_INVESTMENT]
    dfa = df[:,cols]
    gd = groupby(dfa, :FISYR)
    yrs = 1:length(gd) |> Map(i->gd[i]) |> Map(g-> g.FISYR[1]) |> collect
    func(col) = 1:length(gd) |> Map(i->gd[i]) |> Map(g->g[:, col]) |> Map(sum) |> collect
    arr = foldxt(hcat, cols[2:end] |> Map(func))
    arr = hcat(yrs, arr)
    DataFrame(arr, cols)
end
function fixedopt(dfr::DataFrame, grant_rate::Float64; growth_rate = .08)
    a0 = dfr.ASSETS_NET[1]
    donate_steps = dfr.DONATIONS[2:end]
    func(a, d) = ((1 - grant_rate) * a)*(1+growth_rate) + d
    a = donate_steps |> Scan(func, a0) |> collect
    grant_rate .* vcat(a0,a)
end
function randopt(a0, donate_dist, growth_dist; n=20, grantrate)
    donate_percents = rand(donate_dist, n)
    growth_percents = rand(growth_dist, n)
    func(a, donate, grow) = (1 - grantrate) * a * (1 + grow) + donate * a
    func(a, tup::Tuple{Float64, Float64}) = func(a, tup[1], tup[2])
    zip(donate_percents, growth_percents) |> Scan(func, a0) |> collect
end
function montecarlo(args...; ntrials=100000, n=20, applyfunc=false, kwargs...)
    out = typeof(applyfunc) <: Function ? zeros(ntrials) : zeros(n, ntrials)
    @threads for i=1:ntrials
        col = randopt(args...; n=n, kwargs...)
        typeof(applyfunc) <: Function ? out[i] = applyfunc(col) : out[:,i] = col
    end
    out
end

function mcmean(arr)
    n, _ = size(arr)
    m = zeros(n)
    @threads for i=1:n
        m[i] = mean(arr[i,:])
    end
    m
end

function plotmc(dfr::DataFrame, grantrate::Float64; n=20, ntrials=10000)
    x = dfr.FISYR
    donate_dist = fit(Normal, dfr.DONATIONS[2:end] ./ dfr.ASSETS_NET[1:end-1])
    growth_dist = fit(Normal, dfr.REVENUE_INVESTMENT[2:end] ./ dfr.ASSETS_NET[1:end-1])
    yy = montecarlo(dfr.ASSETS_NET[1], donate_dist, growth_dist; grantrate=grantrate, n=n, ntrials=ntrials)
    y = grantrate .* mcmean(yy)
    x,y
end
function plotmc(dfr::DataFrame, grantrates::T; n=20, ntrials=10000, area=false) where {T <: Union{AbstractRange, AbstractArray}}
    x = dfr.FISYR
    donate_dist = fit(Normal, dfr.DONATIONS[2:end] ./ dfr.ASSETS_NET[1:end-1])
    growth_dist = fit(Normal, dfr.REVENUE_INVESTMENT[2:end] ./ dfr.ASSETS_NET[1:end-1])
    a0 = dfr.ASSSES_NET[1]
    x, plotmc(a0, donate_dist, growth_dist, grantrates; n=n, ntrials=ntrials, area=area)
end

 assets_next_year(a₀, Grate, Mrate, Drate) =
     a₀ * (1 - Grate) * (1 + Mrate) + a₀ * Drate

function total_giving(a₀, Grate, Mrate, Drate; n_years, discount = false)
    func(a) = assets_next_year(a, Grate, Mrate, Drate)
    n = round(n_years) |> Int
    assets = 1:n |> Iterated(func, a₀) |> collect
    if typeof(discount) <: Real
        assets = 1:n |> Map(i-> assets[i]/(1+discount)^i) |> collect
    end
    Grate .* assets |> sum
end

export optfunc, optsurface, opt, optplot, optyears, discountplot
optfunc(;a₀, Mrate, Drate, n_years, discount=false) =
    G -> total_giving(a₀, G, Mrate, Drate, n_years=n_years, discount=discount)

function opt(m,d,n_years; discount=false)
    func = optfunc(a₀=1e3, Mrate=m, Drate=d, n_years=n_years, discount=discount)
    f(x) = 100 / func(x...)
    res = bboptimize(f, SearchRange=[(0.0,1.0)], NumDimensions=1, TraceMode=:silent)
    z = best_candidate(res)[1]
end

function optyears(Grate, Mrate, Drate, yearmax=50.0,  discount=false)
    optg = 1:yearmax |> Map(N-> opt(Mrate, Drate, N, discount=discount)) |> tcollect
    xy = zip(1:yearmax, optg) |> collect
    sort!(xy, by=x-> (x[2] - Grate)^2) |> first
end

function optsurface(mrates, drates, n_years; discount=false)
    ll = length(mrates) * length(drates)
    xx = zeros(ll)
    yy = zeros(ll)
    zz = zeros(ll)
    opt_(x,y) = opt(x,y,n_years; discount=discount)
    @threads for i=1:length(mrates)
        x = mrates[i]
        for j = 1:length(drates)
            y = drates[j]
            z = opt_(x,y)
            ii = i*j
            if z > 0
                xx[ii] = x
                yy[ii] = y
            end
            zz[ii] = z
        end
    end
    map([xx,yy,zz]) do v
        filter(x->x>0, v)
    end
end

function gridsearchlines(mrates, drates, nrange; discount=false)
    low , med, high = Float64[],Float64[],Float64[]
    for n in nrange
        _, _, z = optsurface(mrates, drates, n; discount=discount)
        l, m, h = minimum(z), median(z), maximum(z)
        push!(low, l)
        push!(med, m)
        push!(high, h)
    end
    low, med, high
end

function optplot(mrates, drates, nrange; discount=false)
    p = plot(size=(1200,900), xlabel = "Years", ylabel = "Grant Rate", Title = "Optimal Grant Rate")
    optplot(p, mrates, drates, nrange; discount=discount)
end
function optplot(p::Plots.Plot, mrates, drates, nrange; discount=false, light=:pink, dark=:red)
    z1, z2, z3 = gridsearchlines(mrates, drates, nrange; discount=discount)
    plot!(p, nrange, z1, color=light, label="Minimum, Maximum")
    plot!(p, nrange, z2, color=dark, label="Median")
    plot!(p, nrange, z3, color=light, label=false)
    p
end

function discountplot(mrates, drates, nrange; discount_rates)
    p = plot(size=(1200,900), xlabel = "Years", ylabel = "Grant Rate", leg=false)
    colors = [:red, :blue, :black, :purple, :green, :yellow]
    for (i,d) in enumerate(discount_rates)
        ii = mod(length(colors),i)
        _, z, _ = gridsearchlines(mrates, drates, nrange; discount = d)
        plot!(p, nrange, z, color=colors[ii])
    end
    p
end

function stepfunc_helper(a0, donate, grow, grant, n; tax)
    ff(a, donate, grow) = (1 - grant) * a * (1 + grow) * (1 - tax) + donate * a * (1 - tax)
    ff(a, tup::Tuple) = ff(a,tup...)
    [(donate,grow) for i=1:n] |> Scan(ff, a0) |> collect
end


function pmc(a0::Real, applyfunc = identity; n=20, tax=.01)
    return (x,y,z)-> z * stepfunc_helper(a0, x, y, z, n; tax=tax) |> applyfunc
    yya = grantrates |> Map(func) |> tcollect
    typeof(applyfunc) <: Function ? map(applyfunc,yya) : foldl(hcat, yya)
end

function meta_mc(d1_mus, d2_mus,grantrates; n=20)
    a0 = 10e9
    out = []
    for μ1 in d1_mus
        for μ2 in d2_mus
            y = plotmc(a0, μ1, μ2, grantrates; n=n, applyfunc=sum)
            xmax, _ = sort(collect(zip(grantrates,y)), by=x->x[2], rev=true) |> first
            push!(out, (μ1, μ2, xmax))
        end
    end
    out |> unzip
end

function unzip(v::Vector)
    out = [zeros(length(v)) for i=1:length(v[1])]
    for (i,tup) in enumerate(v)
        for (j,x) in enumerate(tup)
            out[j][i] = x
        end
    end
    out
end
