include("gather_data.jl")
mutable struct AssetEntry
    id::UInt64
    asset
    amounts_spent::Vector{Real}
    amount_init::Real
    cost_basis::Real
    time_acquired::DateTime
    spent::Bool
end
AssetEntry(a, amount, basis, ts) = AssetEntry(
    hash(rand(1:1e16)),
    a,
    Real[],
    amount,
    basis,
    ts,
    false
)

struct AssetEvent
    type::Symbol
    asset
    amount::Real
    basis::Real
    timestamp::DateTime
end
alter(ae::AssetEvent, amt) = AssetEvent(
    ae.type,
    ae.asset,
    amt,
    ae.basis,
    ae.timestamp
)
struct Realization
    asset
    quantity::Real
    basis_buy::Real
    date_buy::DateTime
    basis_sell::Real
    date_sell::DateTime
    dollar::Real
end
function inv_init(d_init::Dict)
    id = Dict()
    d = CSV.read("basis.csv", DataFrame)
    foreach(s->currency(Symbol(s)), d.asset)
    for (i, astring) in enumerate(d.asset)
        a = currency(Symbol(astring))
        if a != USD
            vec = get(id, a, AssetEvent[])
            push!(vec, AssetEvent(:purchase, a, d_init[a], d.basis_in_dollars[i], DateTime(tax_year, 1,1)))
            id[a] = vec
        end
    end
    id
end

raw_bittrex = CSV.read("data/BittrexOrderHistory_2021.csv", DataFrame)
#final_balances = CSV.read("data/Bittrex_Final_Balances.csv", DataFrame)

#inv_final = d = Dict(currency(Symbol(k))=>v for (k,v) in zip(final_balances.asset, final_balances.Quantity))
raw_binanceus = CSV.read("data/binanceus_2021.csv", DataFrame)
filter!(raw_binanceus) do row
    row.Category == "Spot Trading"
end

using TimeSeries
eth_price = readtimearray("price_data/ETH-USD.csv")
btc_price = readtimearray("price_data/BTC-USD.csv")

function get_high_val(ts, ta)
    ii = size(to(ta, ts))[1]
    vals = ta[ii:ii+1][:high] |> values
    maximum(vals)
end


function parse_row_bittrex(r)
    quantity = r.Quantity - r.QuantityRemaining
    cost = r.Price
    quote_asset, base_asset = map(currency, map(Symbol, split(r.Exchange, "-")))
    otype = split(r.OrderType, "_")[2]
    ts = DateTime(r.TimeStamp, "m/d/yyyy I:MM:SS p")
    if otype == "SELL"
        quantity = -quantity
        cost = cost - r.Commission
    else
        @assert otype == "BUY"
        cost = -(cost + r.Commission)
    end
    # if not in dollars, usually in BTC or ETH; try to find price of that for basis
    raw_cost = r.Price
    raw_quantity = r.Quantity - r.QuantityRemaining
    parserow((quote_asset, cost), (base_asset, quantity), raw_cost, raw_quantity, ts)
end


function parserow(quote_::Tuple, base::Tuple, raw_cost, raw_quantity, ts::DateTime)
    d = Dict(base[1]=>base[2], quote_[1]=>quote_[2], :timestamp=>ts)
    quote_asset, cost = quote_
    base_asset, quantity = base

    basis = Dict()
    if quote_asset in (USD, USDT)
        basis[base_asset] = raw_cost / raw_quantity
    elseif base_asset in (USD, USDT)
        basis[quote_asset] = raw_quantity / raw_cost
    elseif quote_asset == BTC && raw_quantity > 0
        basis[BTC] = get_high_val(ts, btc_price)
        basis[base_asset] = basis[BTC] * raw_cost / raw_quantity
    elseif base_asset == BTC && raw_cost > 0
        basis[BTC] = get_high_val(ts, btc_price)
        basis[quote_asset] = basis[BTC] * raw_quantity / raw_cost
    elseif quote_asset == ETH && raw_quantity > 0
        basis[ETH] = get_high_val(ts, eth_price)
        basis[base_asset] = basis[ETH] * raw_cost / raw_quantity
    elseif base_asset == ETH && raw_cost > 0
        basis[ETH] = get_high_val(ts, eth_price)
        basis[quote_asset] = basis[ETH] * raw_quantity / raw_cost
    end

    d[:basis] = basis
    d
end

Rows(df::DataFrame) = 1:size(df)[1] |> Map(i -> df[i,:])
rowmap(func::Function, df::DataFrame) = df |> Rows |> Map(func) |> collect

steps_bittrex = rowmap(parse_row_bittrex, raw_bittrex)

function parse_row_binanceus(r)
    quote_asset = currency(r.Quote_Asset)
    raw_cost = r.Realized_Amount_For_Quote_Asset
    cost = raw_cost
    base_asset = currency(r.Base_Asset)
    raw_quantity = r.Realized_Amount_For_Base_Asset
    quantity = raw_quantity
    fee_asset = currency(r.Fee_Asset)
    fee = r.Realized_Amount_For_Fee_Asset
    if r.Operation == "Buy"
        if fee_asset == base_asset
            cost = -raw_cost
            quantity = raw_quantity - fee
        elseif fee_asset == quote_asset
            cost = -(raw_cost + fee)
            quantity = raw_quantity
        else
            cost = -raw_cost
            quantity = raw_quantity
        end
    else
        @assert r.Operation == "Sell"
        if fee_asset == base_asset
            cost = cost
            quantity = -(raw_quantity + fee)
        elseif fee_asset == quote_asset
            cost = cost - fee
            quantity = -quantity
        else # weird fee asset; just ignoring
            cost = cost
            quantity = -quantity
        end
    end
    ts = DateTime(r.Time, "yyyy-mm-dd HH:MM:SS")
    parserow((quote_asset, cost), (base_asset, quantity), raw_cost, raw_quantity, ts)
end
steps_binanceus = rowmap(parse_row_binanceus, raw_binanceus)

raw_kraken = CSV.read("data/trades_kraken_2021.csv", DataFrame)
function kraken_currency(str)
    str = str[2:end]
    str == "XBT" ? BTC : currency(str)
end
function kraken_time(tstring)
    n = split(tstring, ".") |> last |> length
    n3 = max(0, n-3)
    tstring = tstring[1:end-n3]
    DateTime(tstring, "yyyy-mm-dd HH:MM:SS.s")
end
function parse_row_kraken(r)
    quote_asset = r.pair[5:8] |> kraken_currency
    raw_cost = r.cost
    cost = raw_cost
    base_asset = r.pair[1:4] |> kraken_currency
    raw_quantity = r.vol
    fee = r.fee
    if r.type == "buy"
        cost = -(raw_cost + fee)
        quantity = raw_quantity
    else
        @assert r.type == "sell"
        cost = raw_cost - fee
        quantity = -raw_quantity
    end
    ts = kraken_time(r.time)
    parserow((quote_asset, cost), (base_asset, quantity), raw_cost, raw_quantity, ts)
end
steps_kraken = rowmap(parse_row_kraken, raw_kraken)

raw_coinbasepro = CSV.read("data/account_cbpro_2021.csv", DataFrame)
DataFrames.rename!(raw_coinbasepro, 8=>:trade_id, 6=>:unit)
filter!(x->x.type in ("fee", "match"), raw_coinbasepro)
function process_cbpro(df::DataFrame)
    trades = union(df.trade_id)
    map(trades) do t
        d = filter(df) do row
            row.trade_id == t
        end

        @assert size(d)[1] == 3
        sort!(d, [:type, :unit], rev=true)
        asset1, asset2, fee_asset = map(currency, d.unit)
        amt1, amt2, fee = d.amount
        quote_asset, base_asset = if asset1 == USD
            asset1, asset2
        elseif asset2 == USD
            asset2, asset1
        elseif asset1 == BTC
            asset1, asset2
        elseif asset2 == BTC
            asset2, asset1
        elseif asset1 == ETH
            asset1, asset2
        elseif asset2 == ETH
            asset2, asset1
        else
            asset1, asset2
        end
        raw_cost, raw_quantity = asset1 == quote_asset ? (amt1, amt2) : (amt2, amt1)
        cost = raw_cost + fee
        quantity = raw_quantity
        raw_cost = abs(raw_cost)
        raw_quantity = abs(raw_quantity)
        ts = DateTime(d.time[1], "yyyy-mm-ddTHH:MM:SS.sZ")
        parserow((quote_asset, cost), (base_asset, quantity), raw_cost, raw_quantity, ts)
    end
end
steps_cbpro = process_cbpro(raw_coinbasepro)

steps_sfox = [
    parserow((BTC, -0.04549574), (ETH, 1.41691226), 0.04538228,1.41691226, DateTime(2021,1,7,10,5))
]

#bittrex = parse_bittrex() |> find_balance_init
d_init,_ = parse_all() #Dict(b.unit => b.balance for b in bittrex)
steps = vcat(steps_binanceus, steps_bittrex, steps_kraken, steps_cbpro, steps_sfox)

#init

snapshots = []; asdict = Dict(); dlist = [];d = copy(d_init); push!(snapshots, copy(d)); id = inv_init(d_init)


sort!(steps, by=x->x[:timestamp])


function forward(s)
    ts = s[:timestamp]
    for (k,v) in s
        if k != :timestamp && k != :basis
            d[k] += v
            if k != USD
                tt = v > 0 ? :purchase : :sale
                basis = get(s[:basis], k, NaN)
                basis = (isnan(basis) && k==USDT) ? 1.0 : basis
                ae = AssetEvent(tt, k, v, basis, ts)
                kk = get(id, k, AssetEvent[])
                push!(kk, ae)
                id[k] = kk
            else
                push!(dlist, (v, ts))
            end
        end
    end
    push!(snapshots, copy(d))
end

foreach(forward, steps)

function calc(cur, id; time_order=true, verbose=false)
    ii = sort(filter(x->x.amount > 0, id[cur]), by=x->x.timestamp)
    jj = sort(filter(x->x.amount < 0, id[cur]), by=x->x.timestamp)
    r, b, s = best_buy_sell(ii,jj; time_order=time_order)
    tr = sum([x.dollar for x in r])
    verbose ? (tr, r, b, s) : tr
end

function calc_better(cur, id; verbose=true)
    r1 = calc(cur, id; time_order=true, verbose=true)
    r2 = calc(cur, id; time_order=false, verbose=true)
    out = r1[1] < r2[1] ? r1 : r2
    verbose ? out[1] : out
end

calc_all(id; kwargs...) = Dict(k=>calc(k, id; kwargs...) for (k,v) in id)


function find_best_matching(buy_list, sell_list)
    total_remaining = sum([a.amount for a in buy_list]) + sum([a.amount for a in sell_list])
    eligible_buy_list = filter(buy_list) do ae
        ae.timestamp < last(sell_list).timestamp
    end
end

function best_buy_sell(buy_list::Vector{AssetEvent}, sell_list::Vector{AssetEvent}; time_order=false)
    s = copy(sell_list)
    realized, remaining_buys, remaining_sells = best_buy_sell(buy_list, s, Realization[], AssetEvent[]; time_order=time_order)
    # checks
    filter!(x->x.quantity ≠ 0, realized)
    for r in realized
        @assert r.quantity > 0
        @assert r.date_buy ≤ r.date_sell
    end
    @assert sum(map(x->x.quantity, realized)) ≤ sum(map(x->x.amount, buy_list))
    realized, remaining_buys, remaining_sells
end

function best_buy_sell(buy_list::Vector{AssetEvent}, sell_list::Vector{AssetEvent}, realized::Vector{Realization}, leftover_sells::Vector; time_order)
    if length(sell_list) == 0 || length(buy_list) == 0
        return realized, buy_list, leftover_sells
    end

    if !time_order
        ss = sort(sell_list, by=x->x.basis, rev=true)
        #ss = sort(sell_list, by=x->x.timestamp)
        # to check, use this shuffle code, run a bunch of sims, then filter out the results with unused sells
        #ss = shuffle(sell_list)

        ss1 = copy(ss)
        for (i,sell) in enumerate(ss)
            r, b, s = best_buy(buy_list, sell)
            deleteat!(ss1, findall(x->x==sell, ss1))
            if length(ss1) == 0 || length(b) == 0 || check_enough_buys_sells(b, ss1)
                return best_buy_sell(b, ss1, vcat(realized, r), s; time_order=time_order)
            else
                ss1 = copy(ss)
            end
        end
        realized, buy_list, sell_list
    else
        sort!(sell_list, by=x->x.timestamp, rev=true)
        sell = pop!(sell_list)
        r,b,s = best_buy(buy_list, sell)
        best_buy_sell(b, sell_list, vcat(realized, r), s; time_order=time_order)
    end
end

function best_buy(buy_list::Vector{AssetEvent}, sell_entry::AssetEvent)
    buy_exclude = filter(buy_list) do buy
        buy.timestamp > sell_entry.timestamp
    end
    buy_ok = setdiff(buy_list, buy_exclude)
    if length(buy_ok) == 0
        return Realization[], buy_list, [sell_entry]
    end
    rlist, blist, slist = best_buy(buy_ok, sell_entry, Realization[])
    bout = sort(vcat(buy_exclude, blist), by=x->x.timestamp)
    rlist, bout, slist
end

function best_buy(buy_list::Vector{AssetEvent}, sell_entry::AssetEvent, rlist::Vector{Realization})
    bl = sort(buy_list, by=x->x.basis)
    b = pop!(bl)
    realized, event, etype = match_buy_sell(b, sell_entry)
    push!(rlist, realized)
    if length(bl) == 0
        return rlist, AssetEvent[], [event]
    elseif etype == :remaining_buy
        push!(bl, event)
        return rlist, sort(bl, by=b->b.timestamp), AssetEvent[]
    else
        best_buy(bl, event, rlist)
    end
end
function match_buy_sell(buy, sell)
    @assert buy.asset == sell.asset
    amt_realized = min(buy.amount, abs(sell.amount))
    amt_left = buy.amount - amt_realized
    realized = Realization(
        sell.asset,
        amt_realized,
        buy.basis,
        buy.timestamp,
        sell.basis,
        sell.timestamp,
        amt_realized * (sell.basis - buy.basis)
        )
    if amt_left > 0
        new_buy = AssetEvent(
            buy.type,
            buy.asset,
            amt_left,
            buy.basis,
            buy.timestamp
        )

        realized, new_buy, :remaining_buy
    else
        new_sell = AssetEvent(
            sell.type,
            sell.asset,
            buy.amount + sell.amount,
            sell.basis,
            sell.timestamp
        )
        realized, new_sell, :remaining_sell
    end
end

function tp(q::Real,p::Real, dt::DateTime)
    AssetEvent(
        :purchase,
        ETH,
        q,p,dt
    )
end
tp(dt::DateTime, args...) = t_(dt, tp, args...)
t_(dt::DateTime, func::Function, args...) = map(args) do tup
    q,p = tup
    func(q,p,dt)
end |> collect

function ts(q::Real,p::Real, dt::DateTime)
    AssetEvent(
        :sale,
        ETH,
        -q,p,dt
    )
end
ts(dt::DateTime, args...) = t_(dt, ts, args...)

sumq(v::Vector{AssetEvent}) = sum([x.amount for x in v]) |> abs
sumq(v::Vector{Realization}) = sum([x.dollar for x in v])

function check_enough_buys_sells(buy_list::Vector{AssetEvent}, sell_list::Vector{AssetEvent})
    sort!(buy_list, by=x->x.timestamp)
    sort!(sell_list, by=x->x.timestamp)

    function func(s)
        tblist = filter(x->x.timestamp < s.timestamp, buy_list)
        tslist = filter(x->x.timestamp ≤ s.timestamp, sell_list)
        sumq(tblist) ≥ sumq(tslist)
    end
    foldl(&, sell_list |> Map(func), init=true)
end

function sumof(vector)
    buys = []
    sells = []
    foreach(vector) do d
        for (k,v) in d
            if k != :basis && k!= :timestamp && k != USD && k != USDT
                vv = v * d[:basis][k]
                vv > 0 ? push!(buys, vv) : push!(sells, vv)
            end
        end
    end
    sum(buys), sum(sells)
end
function display(r::Realization)
    println("Date sold:     ", floor(r.date_buy, Day))
    println("Date acquired: ", floor(r.date_sell, Day))
    println("Sale proceeds: \$", round(r.basis_sell * r.quantity, digits = 2))
    println("Cost basis:    \$", round(r.basis_buy * r.quantity, digits = 2))
    println("Short term?    ", r.date_sell - r.date_buy < Millisecond(1e3*60*60*24*365))
    println("----------------------------------------")
end

function make_csv(dict)
    df = DataFrame(description = [], date_acquired=[], date_sold=[], proceeds=[], cost_basis=[], net_gain=[], datetime_bought=[], datetime_sold=[])
    for (k,v) in dict
        foreach(v[2]) do r
            push!(df, (k, floor(r.date_buy, Day)|>Date, floor(r.date_sell, Day)|>Date, round(r.basis_sell * r.quantity, digits=2), round(r.basis_buy * r.quantity, digits=2),round(r.dollar, digits=2), r.date_buy, r.date_sell))
        end
    end
    sort!(df, [:date_sold, :date_acquired])
    CSV.write("cost_basis.csv", df)
    df
end

function make_txf(df::DataFrame, name="cost_basis.txf")
    dformat(dt) = Dates.format(dt, dateformat"mm/dd/yyyy")
    open(name, "w") do f
        write(f, "V042\n")
        write(f, "ACryptoTaxes\n")
        write(f, string("D ", dformat(today()), "\n"))
        write(f, "^\n")
        for row in eachrow(df)
            write(f, "TD\n")
            write(f, "N712\n")
            write(f, "C1\n")
            write(f, "L1\n")
            write(f, string("P ", row.description, "\n"))
            write(f, string("D", Dates.format(row.date_acquired, dateformat"mm/dd/yyyy"), "\n"))
            write(f, string("D", Dates.format(row.date_sold, dateformat"mm/dd/yyyy"), "\n"))
            write(f, string("\$", row.cost_basis , "\n"))
            write(f, string("\$", row.proceeds, "\n"))
            write(f, "^\n")
        end
    end
end
